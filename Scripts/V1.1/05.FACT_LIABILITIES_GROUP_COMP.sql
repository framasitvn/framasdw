USE framasDW
GO

IF EXISTS (SELECT NULL FROM SYS.TABLES WHERE NAME = 'MR_LIABILITIES_GROUP')
	DROP TABLE staging.MR_LIABILITIES_GROUP;
GO

CREATE TABLE staging.MR_LIABILITIES_GROUP (
	LG_FACTORY_KEY	SMALLINT,
	LG_DATE			DATE,
	LG_ACCOUNT_ID	INT,	
	LG_AMOUNT		DECIMAL(18,4),
	LG_SCENARIO_ID	TINYINT,
	LG_ROLLING_DATE	DATE)
GO


IF EXISTS (SELECT NULL FROM SYS.TABLES WHERE NAME = 'FACT_LIABILITIES_GROUP')
	DROP TABLE FACT_LIABILITIES_GROUP;
GO

CREATE TABLE FACT_LIABILITIES_GROUP (
	FLG_KEY				BIGINT IDENTITY(1,1) NOT NULL,
	FLG_DF_FACTORY_KEY	INT NOT NULL,
	FLG_DATE			DATE NOT NULL,	
	FLG_REPORT_DATE		DATE NOT NULL,	
	FLG_AMOUNT			DECIMAL(18,4) DEFAULT (0), 	
	FLG_SCENARIO_ID		TINYINT NOT NULL,
	FLG_CRE_DATE		DATETIME NOT NULL DEFAULT(GETDATE()),
	CONSTRAINT PK_TBL_FACT_LIABILITIES_GROUP PRIMARY KEY (FLG_KEY))	
GO

CREATE INDEX TBL_FACT_LIABILITIES_GROUP_IDX_01
ON FACT_LIABILITIES_GROUP (FLG_DF_FACTORY_KEY, FLG_DATE)
INCLUDE (FLG_REPORT_DATE, FLG_SCENARIO_ID, FLG_CRE_DATE);
GO

IF EXISTS (SELECT NULL FROM SYS.TABLES WHERE NAME = 'FACT_CUR_LIAB_GROUP')
	DROP TABLE FACT_CUR_LIAB_GROUP;
GO

CREATE TABLE FACT_CUR_LIAB_GROUP (	
	FCLG_DF_FACTORY_KEY	INT NOT NULL,
	FCLG_DATE			DATE NOT NULL,			
	FCLG_BUDGET_AMT		DECIMAL(18,4) DEFAULT (0),
	FCLG_ROLL_PLAN_AMT	DECIMAL(18,4) DEFAULT (0)	
)
GO

CREATE INDEX TBL_FACT_CUR_LIAB_GROUP_IDX_01 
ON FACT_CUR_LIAB_GROUP (FCLG_DF_FACTORY_KEY, FCLG_DATE);

IF EXISTS (SELECT NULL FROM SYS.procedures WHERE NAME = 'PROC_FACT_CUR_LIAB_GROUP')
	DROP PROC PROC_FACT_CUR_LIAB_GROUP;
GO

CREATE PROC PROC_FACT_CUR_LIAB_GROUP 
AS 	
	DECLARE @V_LIABILITIES TABLE (
		FACTORY_KEY		INT,
		LIA_DATE		DATE);
BEGIN
	SET NOCOUNT ON;
	--CLEAR DATA ON FACT_CUR_LIAB_GROUP FOR CURRENT REPORT
	TRUNCATE TABLE FACT_CUR_LIAB_GROUP;

	----GET LAST REPORT DATE OF EARCH LOCATION
	INSERT INTO @V_LIABILITIES
	SELECT FLG_DF_FACTORY_KEY, FLG_REPORT_DATE	
	FROM (
		SELECT FLG_DF_FACTORY_KEY, FLG_REPORT_DATE, ROW_NUMBER() OVER(PARTITION BY FLG_DF_FACTORY_KEY ORDER BY FLG_REPORT_DATE DESC) _RANK					
		FROM FACT_LIABILITIES_GROUP		
		GROUP BY FLG_DF_FACTORY_KEY, FLG_REPORT_DATE) TMP
	WHERE TMP._RANK = 2;	

	--ADD LIABILITIES DATA FOR CURRENT REPORT
	INSERT INTO FACT_CUR_LIAB_GROUP (
		FCLG_DF_FACTORY_KEY,			FCLG_DATE,				FCLG_BUDGET_AMT,				FCLG_ROLL_PLAN_AMT
	)
	SELECT PVT.FLG_DF_FACTORY_KEY,		PVT.FLG_DATE,			ISNULL(PVT.[2], 0) BUDGET_AMT,	ISNULL(PVT.[1], 0) ROLL_PLAN_AMT
	FROM (
		SELECT 
			LG.FLG_DF_FACTORY_KEY,	LG.FLG_DATE, CASE WHEN FLG_SCENARIO_ID != 2 THEN 1 ELSE 2 END FLG_TYPE, ISNULL(SUM(LG.FLG_AMOUNT), 0) FLG_AMOUNT
		FROM FACT_LIABILITIES_GROUP LG
			INNER JOIN @V_LIABILITIES L 
				ON LG.FLG_DF_FACTORY_KEY = L.FACTORY_KEY 
				AND  
					(LG.FLG_SCENARIO_ID = 2 
					OR 
						( (LG.FLG_SCENARIO_ID = 1 AND YEAR(LG.FLG_DATE) = YEAR(L.LIA_DATE) AND LG.FLG_DATE < L.LIA_DATE)
					   OR (LG.FLG_SCENARIO_ID = 3 AND YEAR(LG.FLG_DATE) = YEAR(L.LIA_DATE) AND LG.FLG_REPORT_DATE = L.LIA_DATE)
						)
					)
		GROUP BY LG.FLG_DF_FACTORY_KEY, LG.FLG_DATE, CASE WHEN FLG_SCENARIO_ID != 2 THEN 1 ELSE 2 END) T1
	PIVOT (SUM(T1.FLG_AMOUNT) FOR FLG_TYPE IN ([1],[2])) PVT;	
END
GO

IF EXISTS (SELECT NULL FROM SYS.procedures WHERE NAME = 'PROC_FACT_LIABILITIES_GROUP')
	DROP PROC PROC_FACT_LIABILITIES_GROUP;
GO

CREATE PROC PROC_FACT_LIABILITIES_GROUP @P_CUR_DATE NVARCHAR(20) = NULL
AS 	
BEGIN
	SET NOCOUNT ON;
	
	IF ISNULL(@P_CUR_DATE, '') = ''
		TRUNCATE TABLE FACT_LIABILITIES_GROUP;
	ELSE
	BEGIN		
		DELETE FL
		FROM FACT_LIABILITIES_GROUP FL
			INNER JOIN ##LIABILITIES_GROUP LG 
		ON FL.FLG_DF_FACTORY_KEY = LG.LG_FACTORY_KEY AND FL.FLG_REPORT_DATE = LG.LG_ROLLING_DATE;
	END
	---ADD DATA FOR LIABILITIES GROUP COMPANY
	INSERT INTO FACT_LIABILITIES_GROUP (			
		FLG_DF_FACTORY_KEY,				FLG_DATE,				FLG_REPORT_DATE,				FLG_AMOUNT,
		FLG_SCENARIO_ID	
	)
	SELECT 
		LG.LG_FACTORY_KEY,				LG.LG_DATE,				LG.LG_ROLLING_DATE,				LG.LG_AMOUNT,
		LG.LG_SCENARIO_ID
	FROM ##LIABILITIES_GROUP  LG

	----GET DATA FOR CURRENT REPORT
	EXEC PROC_FACT_CUR_LIAB_GROUP;
END
	
GO