USE framasDW
GO

IF EXISTS (SELECT NULL FROM SYS.views WHERE NAME = 'VW_KEYPOINT_TYPE')
	DROP VIEW VW_KEYPOINT_TYPE;
GO

CREATE VIEW VW_KEYPOINT_TYPE AS
	SELECT 0 KPT_ID, '' KPT_NAME
	UNION ALL 
	SELECT 1 KPT_ID, 'Increasing' KPT_NAME
	UNION ALL 
	SELECT 2 KPT_ID, 'Decreasing' KPT_NAME
GO

IF EXISTS (SELECT NULL FROM SYS.views WHERE NAME = 'VW_TARGET_PLAN_TYPE')
	DROP VIEW VW_TARGET_PLAN_TYPE;
GO

CREATE VIEW VW_TARGET_PLAN_TYPE AS	
	SELECT * 
	FROM 
		(SELECT CAST(1 AS TINYINT) TGP_TYPE_ID, 'Target' TGP_TYPE_NAME UNION ALL 
		 SELECT CAST(2 AS TINYINT) TGP_TYPE_ID, 'Plan' TGP_TYPE_NAME
		) VW
GO


IF EXISTS (SELECT NULL FROM SYS.views WHERE NAME = 'VW_INCOME_STATEMENT_TYPE')
	DROP VIEW VW_INCOME_STATEMENT_TYPE;
GO

CREATE VIEW VW_INCOME_STATEMENT_TYPE AS	
	SELECT * 
	FROM 
		(SELECT CAST(1 AS TINYINT) IST_TYPE_ID, 'Current RP' IST_TYPE_NAME UNION ALL 
		 SELECT CAST(2 AS TINYINT) IST_TYPE_ID, 'Budget' IST_TYPE_NAME UNION ALL 
		 SELECT CAST(3 AS TINYINT) IST_TYPE_ID, 'Last RP' IST_TYPE_NAME UNION ALL 
		 SELECT CAST(4 AS TINYINT) IST_TYPE_ID, 'Last Year'   IST_TYPE_NAME
		) VW
GO

IF EXISTS (SELECT NULL FROM SYS.views WHERE NAME = 'VW_TARGET_PLAN_ACCOUNT')
	DROP VIEW VW_TARGET_PLAN_ACCOUNT;
GO

CREATE VIEW VW_TARGET_PLAN_ACCOUNT AS	
	SELECT * 
	FROM 
		(SELECT CAST(1 AS TINYINT) TGP_ACCT_ID, 'Sales'				TGP_ACCT_NAME UNION ALL
		 SELECT CAST(2 AS TINYINT) TGP_ACCT_ID, 'Gross Profit'		TGP_ACCT_NAME UNION ALL
		 SELECT CAST(3 AS TINYINT) TGP_ACCT_ID, 'EBIT'				TGP_ACCT_NAME UNION ALL
		 SELECT CAST(4 AS TINYINT) TGP_ACCT_ID, 'EBITDA'			TGP_ACCT_NAME UNION ALL
		 SELECT CAST(5 AS TINYINT) TGP_ACCT_ID, 'Net Investment'	TGP_ACCT_NAME UNION ALL
		 SELECT CAST(6 AS TINYINT) TGP_ACCT_ID, 'Dividend Payment'	TGP_ACCT_NAME UNION ALL
		 SELECT CAST(7 AS TINYINT) TGP_ACCT_ID, 'Loan Repayment'	TGP_ACCT_NAME UNION ALL
		 SELECT CAST(8 AS TINYINT) TGP_ACCT_ID, 'Credit Limit'		TGP_ACCT_NAME
	) VW
GO

IF EXISTS (SELECT NULL FROM SYS.views WHERE NAME = 'VW_INCOME_STATEMENT_ACCOUNT')
	DROP VIEW VW_INCOME_STATEMENT_ACCOUNT;
GO

CREATE VIEW VW_INCOME_STATEMENT_ACCOUNT AS	
	SELECT * 
	FROM 
		(SELECT CAST(1	AS TINYINT) ISA_ACCT_ID, 'Total Sales'					ISA_ACCT_NAME UNION ALL
		SELECT CAST(2	AS TINYINT) ISA_ACCT_ID, 'Material Expense'				ISA_ACCT_NAME UNION ALL
		SELECT CAST(3	AS TINYINT) ISA_ACCT_ID, 'Wages & Salaries (COGS)'		ISA_ACCT_NAME UNION ALL
		SELECT CAST(4	AS TINYINT) ISA_ACCT_ID, 'Depreciation (COGS)'			ISA_ACCT_NAME UNION ALL
		SELECT CAST(5	AS TINYINT) ISA_ACCT_ID, 'Repair & Maintenance (COGS)'	ISA_ACCT_NAME UNION ALL
		SELECT CAST(6	AS TINYINT) ISA_ACCT_ID, 'Utilities (COGS)'				ISA_ACCT_NAME UNION ALL
		SELECT CAST(7	AS TINYINT) ISA_ACCT_ID, 'Subcontractor'				ISA_ACCT_NAME UNION ALL
		SELECT CAST(8	AS TINYINT) ISA_ACCT_ID, 'Change in Stock'				ISA_ACCT_NAME UNION ALL
		SELECT CAST(9	AS TINYINT) ISA_ACCT_ID, 'Other Factory Overhead'		ISA_ACCT_NAME UNION ALL
		SELECT CAST(10	AS TINYINT) ISA_ACCT_ID, 'Gross Profit'					ISA_ACCT_NAME UNION ALL
		SELECT CAST(11	AS TINYINT) ISA_ACCT_ID, 'Selling Expense'				ISA_ACCT_NAME UNION ALL
		SELECT CAST(12	AS TINYINT) ISA_ACCT_ID, 'Administrative Expense'		ISA_ACCT_NAME UNION ALL
		SELECT CAST(13	AS TINYINT) ISA_ACCT_ID, 'Other Operating Income'		ISA_ACCT_NAME UNION ALL
		SELECT CAST(14	AS TINYINT) ISA_ACCT_ID, 'Other Operating Expense'		ISA_ACCT_NAME UNION ALL
		SELECT CAST(15	AS TINYINT) ISA_ACCT_ID, 'Operating Profit (EBIT)'		ISA_ACCT_NAME UNION ALL
		SELECT CAST(16	AS TINYINT) ISA_ACCT_ID, 'EBITDA'						ISA_ACCT_NAME UNION ALL
		SELECT CAST(17	AS TINYINT) ISA_ACCT_ID, 'Financial Result'				ISA_ACCT_NAME UNION ALL
		SELECT CAST(18	AS TINYINT) ISA_ACCT_ID, 'Earnings before tax (EBT)'	ISA_ACCT_NAME UNION ALL
		SELECT CAST(19	AS TINYINT) ISA_ACCT_ID, 'Income Taxes'					ISA_ACCT_NAME UNION ALL
		SELECT CAST(20	AS TINYINT) ISA_ACCT_ID, 'Net income / net loss'		ISA_ACCT_NAME 
	) VW
GO


IF EXISTS (SELECT NULL FROM SYS.views WHERE NAME = 'VW_BALANCE_SHEET_ACCOUNT')
	DROP VIEW VW_BALANCE_SHEET_ACCOUNT;
GO

CREATE VIEW VW_BALANCE_SHEET_ACCOUNT AS	
	SELECT * 
	FROM (
		SELECT CAST( 1  AS TINYINT)	BS_ACCT_ID, 'Prepaid Expenses' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 2  AS TINYINT)	BS_ACCT_ID, 'Cash' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 3  AS TINYINT)	BS_ACCT_ID, 'Receivables and other assets (incl. deferred taxes)' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 4  AS TINYINT)	BS_ACCT_ID, 'short term Receivables/financial assets (<1 year)' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 5  AS TINYINT)	BS_ACCT_ID, 'Inventory' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 6  AS TINYINT)	BS_ACCT_ID, 'Financial Assets' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 7  AS TINYINT)	BS_ACCT_ID, 'Tangible Assets' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 8  AS TINYINT)	BS_ACCT_ID, 'Intangible Assets' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 9  AS TINYINT)	BS_ACCT_ID, 'TOTAL Assets' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 10 AS TINYINT)	BS_ACCT_ID, 'Liabilities to banks' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 11 AS TINYINT)	BS_ACCT_ID, 'Received payments on orders' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 12 AS TINYINT)	BS_ACCT_ID, 'Trade Liabilities' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 13 AS TINYINT)	BS_ACCT_ID, 'Liabilities to group companies' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 14 AS TINYINT)	BS_ACCT_ID, 'Loans with group companies' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 15 AS TINYINT)	BS_ACCT_ID, 'Other Liabilities (incl. deferred taxes)' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 16 AS TINYINT)	BS_ACCT_ID, 'Short term Liabilities (<1 year)' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 17 AS TINYINT)	BS_ACCT_ID, 'Accurals' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 18 AS TINYINT)	BS_ACCT_ID, 'Equity' BS_ACCT_NAME UNION ALL 
		SELECT CAST( 19 AS TINYINT)	BS_ACCT_ID, 'TOTAL Liabilities + Equity' BS_ACCT_NAME
	) BS
GO

