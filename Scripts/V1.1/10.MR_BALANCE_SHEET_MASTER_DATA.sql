USE framasApps
GO

IF EXISTS (SELECT NULL FROM TBL_MASTER WHERE MAS_TYPE = 'MR_BALANCE_SHEET')
	DELETE TBL_MASTER WHERE MAS_TYPE = 'MR_BALANCE_SHEET';
GO

INSERT INTO TBL_MASTER (MAS_TYPE, MAS_CODE, MAS_NAME, MAS_DESCRIPTION, MAS_EXTEND_VALUE, MAS_EXTEND_VALUE2, MAS_CRE_BY)
SELECT 
	'MR_BALANCE_SHEET'		MAS_TYPE,
	FM_FACTORY_ID			MAS_CODE,
	FM_INDEX				MAS_NAME,
	FM_POSITION				MAS_DESCRIPTION,
	CASE WHEN FM_POSITION IN ('2. Prepaid Expenses') THEN 1 
		WHEN FM_POSITION IN ('I. Cash and Banks') THEN 2
		WHEN FM_POSITION IN ('III. Accounts Receivables & Other Assets', '1. Deffered Tax Assets') THEN 3
		WHEN FM_POSITION IN ('1. Trade Receivables','2. Receivables from group companies','3. Trade Receivables from group companies','Loans to group companies < 1 Year') THEN 4
		WHEN FM_POSITION IN ('IV. Inventories') THEN 5
		WHEN FM_POSITION IN ('I. Financial Assets') THEN 6
		WHEN FM_POSITION IN ('II. Tangible Assets') THEN 7
		WHEN FM_POSITION IN ('III. Intangible Assets') THEN 8
		WHEN FM_POSITION IN ('2. Liabilities to Banks') THEN 10
		WHEN FM_POSITION IN ('3. Received payments on orders') THEN 11
		WHEN FM_POSITION IN ('4. Trade Payables') THEN 12
		WHEN FM_POSITION IN ('6. Liabilities to group companies ','Trade liabilities to group companies') THEN 13
		WHEN FM_POSITION IN ('Loans with group companies') THEN 14
		WHEN FM_POSITION IN ('8. Other Liabilities') THEN 15
		WHEN FM_POSITION IN ('C. Accruals') THEN 17
		WHEN FM_POSITION IN ('D. Equity','VI. Profit / Loss', 'Changes due to exchange rate ') THEN 18		
	ELSE 0 END				MAS_EXTEND_VALUE,
	CASE WHEN FM_POSITION IN (
				'2. Prepaid Expenses'
				,'I. Cash and Banks'
				,'III. Accounts Receivables & Other Assets', '1. Deffered Tax Assets'
				,'1. Trade Receivables','2. Receivables from group companies','3. Trade Receivables from group companies','Loans to group companies < 1 Year'
				,'IV. Inventories'
				,'I. Financial Assets'
				,'II. Tangible Assets'
				,'III. Intangible Assets') THEN 1
		WHEN FM_POSITION IN (
				'2. Liabilities to Banks'
				,'3. Received payments on orders'
				,'4. Trade Payables'
				,'6. Liabilities to group companies ','Trade liabilities to group companies'
				,'Loans with group companies'
				,'8. Other Liabilities'
				,'C. Accruals'
				,'D. Equity','VI. Profit / Loss', 'Changes due to exchange rate ') THEN 2
	ELSE 0 END						MAS_EXTEND_VALUE2,
	FM_CRE_BY				MAS_CRE_BY
FROM TBL_MR_FACTORY_MASTER
WHERE 
FM_MASTER_TYPE = 'BS_ACCOUNT'
AND FM_POSITION IN (
			'2. Prepaid Expenses', 
			'I. Cash and Banks',
			'III. Accounts Receivables & Other Assets', '1. Deffered Tax Assets',
			'1. Trade Receivables','2. Receivables from group companies','3. Trade Receivables from group companies','Loans to group companies < 1 Year',
			'IV. Inventories',
			'I. Financial Assets',
			'II. Tangible Assets',
			'III. Intangible Assets',
			--9
			'2. Liabilities to Banks',
			'3. Received payments on orders',
			'4. Trade Payables',
			'6. Liabilities to group companies ','Trade liabilities to group companies',
			'Loans with group companies',
			'8. Other Liabilities',
			--12+13+15,
			'C. Accruals',
			'D. Equity','VI. Profit / Loss', 'Changes due to exchange rate ')
GO