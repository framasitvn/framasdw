USE framasDW
GO

IF EXISTS (SELECT NULL FROM SYS.TABLES WHERE NAME = 'MR_INCOME_STATEMENT')
	DROP TABLE staging.MR_INCOME_STATEMENT;
GO

CREATE TABLE staging.MR_INCOME_STATEMENT (
	TGP_FACTORY_KEY		SMALLINT,
	TGP_DATE			DATE,
	TGP_ACCOUNT_ID		TINYINT,
	TGP_ACCOUNT_NAME	NVARCHAR(255),
	TGP_AMOUNT			DECIMAL(18,4),
	TGP_SCENARIO_ID		TINYINT,
	TGP_ROLLING_DATE	DATE)
GO


IF EXISTS (SELECT NULL FROM SYS.TABLES WHERE NAME = 'FACT_INCOME_STATEMENT')
	DROP TABLE FACT_INCOME_STATEMENT;
GO

CREATE TABLE FACT_INCOME_STATEMENT (
	FIS_KEY					BIGINT IDENTITY(1,1) NOT NULL,
	FIS_DF_FACTORY_KEY		INT NOT NULL,
	FIS_DATE				DATE NOT NULL,
	--FIS_TYPE				TINYINT NOT NULL,
	FIS_REPORT_DATE			DATE NOT NULL,
	--FIS_ACCOUNT_ID			SMALLINT NOT NULL,
	FIS_SALES				DECIMAL(18,4) DEFAULT (0), --Total Sales'					
	FIS_MATERIAL_EXPENSE	DECIMAL(18,4) DEFAULT (0), --Material Expense'				
	FIS_WAGES_SALARY		DECIMAL(18,4) DEFAULT (0), --Wages & Salaries (COGS)'		
	FIS_DEPRECIATION		DECIMAL(18,4) DEFAULT (0), --Depreciation (COGS)'			
	FIS_REPAIR_MAINTENANCE	DECIMAL(18,4) DEFAULT (0), --Repair & Maintenance (COGS)'	
	FIS_UTILITY				DECIMAL(18,4) DEFAULT (0), --Utilities (COGS)'				
	FIS_SUBCONTRACTOR		DECIMAL(18,4) DEFAULT (0), --Subcontractor'				
	FIS_CHANGE_IN_STOCK		DECIMAL(18,4) DEFAULT (0), --Change in Stock'				
	FIS_FACTORY_OVERHEAD	DECIMAL(18,4) DEFAULT (0), --Other Factory Overhead'		
	FIS_GROSS_PROFIT		DECIMAL(18,4) DEFAULT (0), --Gross Profit'					
	FIS_SELLING_EXPENSE		DECIMAL(18,4) DEFAULT (0), --Selling Expense'				
	FIS_ADMIN_EXPENSE		DECIMAL(18,4) DEFAULT (0), --Administrative Expense'		
	FIS_OPERATING_INCOME	DECIMAL(18,4) DEFAULT (0), --Other Operating Income'		
	FIS_OPERATING_EXPENSE	DECIMAL(18,4) DEFAULT (0), --Other Operating Expense'		
	FIS_OPERATING_PROFIT	DECIMAL(18,4) DEFAULT (0), --Operating Profit (EBIT)'		
	FIS_EBITDA				DECIMAL(18,4) DEFAULT (0), --EBITDA'						
	FIS_FINANCIAL_RESULT	DECIMAL(18,4) DEFAULT (0), --Financial Result'				
	FIS_EARNING_BF_TAX		DECIMAL(18,4) DEFAULT (0), --Earnings before tax (EBT)'	
	FIS_INCOME_TAX			DECIMAL(18,4) DEFAULT (0), --Income Taxes'					
	FIS_NET_INCOME_LOSS		DECIMAL(18,4) DEFAULT (0), --Net income / net loss'		
	FIS_SCENARIO_ID			TINYINT NOT NULL,
	FIS_SYS_ADD_DATE		INT NOT NULL,
	FIS_CRE_DATE			DATETIME NOT NULL DEFAULT(GETDATE()),
	CONSTRAINT PK_TBL_FACT_INCOME_STATEMENT PRIMARY KEY (FIS_KEY))	
GO

CREATE INDEX TBL_FACT_INCOME_STATEMENT_IDX_01 
ON FACT_INCOME_STATEMENT (FIS_DF_FACTORY_KEY, FIS_DATE) --, FIS_TYPE)
	INCLUDE (FIS_REPORT_DATE, FIS_SYS_ADD_DATE);

IF EXISTS (SELECT NULL FROM SYS.procedures WHERE NAME = 'PROC_FACT_INCOME_STATEMENT')
	DROP PROC PROC_FACT_INCOME_STATEMENT;
GO

CREATE PROC PROC_FACT_INCOME_STATEMENT @P_CUR_DATE NVARCHAR(20) = NULL
AS 
	DECLARE @V_SYS_ADD_DATE	INT;
	--DECLARE @V_YEAR INT;	
BEGIN
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;
	SET @V_SYS_ADD_DATE	= CONVERT(INT, CONVERT(NVARCHAR(8), GETDATE(), 112));
	
	IF ISNULL(@P_CUR_DATE, '') = ''
		TRUNCATE TABLE FACT_INCOME_STATEMENT;
	ELSE
	BEGIN
		--PRINT('DELETE FIS FROM FACT_INCOME_STATEMENT FIS NEED TO DEFINE FORMULA')
		DELETE FIS
		FROM FACT_INCOME_STATEMENT FIS
			INNER JOIN ##INCOME_STATEMENT INC 
		ON FIS.FIS_DF_FACTORY_KEY = INC.TGP_FACTORY_KEY AND FIS.FIS_REPORT_DATE = INC.TGP_ROLLING_DATE;
	END
	
	--SELECT @V_YEAR = YEAR(MIN(TGP_ROLLING_DATE)) 
	--FROM staging.MR_INCOME_STATEMENT 
	--WHERE TGP_SCENARIO_ID = 3;
		
	----ADD BUDGET DATA TYPE FIS_TYPE = 2
	INSERT INTO FACT_INCOME_STATEMENT (
		FIS_DF_FACTORY_KEY,				FIS_DATE,							FIS_REPORT_DATE,
		FIS_SALES,						FIS_MATERIAL_EXPENSE,				FIS_WAGES_SALARY,					FIS_DEPRECIATION,
		FIS_REPAIR_MAINTENANCE,			FIS_UTILITY,						FIS_SUBCONTRACTOR,					FIS_CHANGE_IN_STOCK,
		FIS_FACTORY_OVERHEAD,			FIS_GROSS_PROFIT,					FIS_SELLING_EXPENSE,				FIS_ADMIN_EXPENSE,
		FIS_OPERATING_INCOME,			FIS_OPERATING_EXPENSE,				FIS_OPERATING_PROFIT,				FIS_EBITDA,
		FIS_FINANCIAL_RESULT,			FIS_EARNING_BF_TAX,					FIS_INCOME_TAX,						FIS_NET_INCOME_LOSS,
		FIS_SCENARIO_ID,				FIS_SYS_ADD_DATE		
	)
	SELECT 
		TGP_FACTORY_KEY,				TGP_DATE,							TGP_ROLLING_DATE,
		SUM(ISNULL([Total Sales],				  0)) FIS_SALES,					
		SUM(ISNULL([Material Expense],			  0)) FIS_MATERIAL_EXPENSE,				
		SUM(ISNULL([Wages & Salaries (COGS)],	  0)) FIS_WAGES_SALARY,		
		SUM(ISNULL([Depreciation (COGS)],		  0)) FIS_DEPRECIATION,			
		SUM(ISNULL([Repair & Maintenance (COGS)], 0)) FIS_REPAIR_MAINTENANCE,	
		SUM(ISNULL([Utilities (COGS)],			  0)) FIS_UTILITY,				
		SUM(ISNULL([Subcontractor],				  0)) FIS_SUBCONTRACTOR,					
		SUM(ISNULL([Change in Stock],			  0)) FIS_CHANGE_IN_STOCK,				
		SUM(ISNULL([Other Factory Overhead],	  0)) FIS_FACTORY_OVERHEAD,		
		SUM(ISNULL([Gross Profit],				  0)) FIS_GROSS_PROFIT,					
		SUM(ISNULL([Selling Expense],			  0)) FIS_SELLING_EXPENSE,				
		SUM(ISNULL([Administrative Expense],	  0)) FIS_ADMIN_EXPENSE,		
		SUM(ISNULL([Other Operating Income],	  0)) FIS_OPERATING_INCOME,		
		SUM(ISNULL([Other Operating Expense],	  0)) FIS_OPERATING_EXPENSE,		
		SUM(ISNULL([Operating Profit (EBIT)],	  0)) FIS_OPERATING_PROFIT,		
		SUM(ISNULL([EBITDA],					  0)) FIS_EBITDA,						
		SUM(ISNULL([Financial Result],			  0)) FIS_FINANCIAL_RESULT,				
		SUM(ISNULL([Earnings before tax (EBT)],	  0)) FIS_EARNING_BF_TAX,		
		SUM(ISNULL([Income Taxes ],				  0)) FIS_INCOME_TAX,					
		SUM(ISNULL([Net income / net loss],		  0)) FIS_NET_INCOME_LOSS,
		TGP_SCENARIO_ID,
		@V_SYS_ADD_DATE
	FROM
		(SELECT TGP_FACTORY_KEY, TGP_DATE, TGP_ROLLING_DATE, TGP_ACCOUNT_NAME, TGP_AMOUNT, TGP_SCENARIO_ID
		 FROM ##INCOME_STATEMENT
		 --WHERE ((YEAR(TGP_DATE) < @V_YEAR AND TGP_SCENARIO_ID = 1) OR TGP_SCENARIO_ID = 2)
			-- --AND YEAR(TGP_DATE) > 2017 
		 --UNION ALL
		 --SELECT M.TGP_FACTORY_KEY, TGP_DATE, A.TGP_ROLLING_DATE, TGP_ACCOUNT_NAME, TGP_AMOUNT
		 --FROM 
			--(SELECT * FROM ##INCOME_STATEMENT	WHERE YEAR(TGP_DATE) = @V_YEAR ) M
			--	INNER JOIN (SELECT DISTINCT TGP_FACTORY_KEY, TGP_ROLLING_DATE 
			--				FROM ##INCOME_STATEMENT 
			--				WHERE YEAR(TGP_ROLLING_DATE) = @V_YEAR AND TGP_SCENARIO_ID  = 3) A
			--ON M.TGP_FACTORY_KEY = A.TGP_FACTORY_KEY 
			--	AND ((M.TGP_ROLLING_DATE < A.TGP_ROLLING_DATE AND M.TGP_SCENARIO_ID = 1) 
			--	 OR  (M.TGP_ROLLING_DATE = A.TGP_ROLLING_DATE AND M.TGP_SCENARIO_ID = 3))
		) TP1
	PIVOT (SUM(TGP_AMOUNT) FOR TGP_ACCOUNT_NAME IN (
		[Total Sales],					[Material Expense],					[Wages & Salaries (COGS)],			[Depreciation (COGS)],
		[Repair & Maintenance (COGS)],	[Utilities (COGS)],					[Subcontractor],					[Change in Stock],
		[Other Factory Overhead],		[Gross Profit],						[Selling Expense],					[Administrative Expense],
		[Other Operating Income],		[Other Operating Expense],			[Operating Profit (EBIT)],			[EBITDA],
		[Financial Result],				[Earnings before tax (EBT)],		[Income Taxes ],					[Net income / net loss]
		)
	) PVT
	GROUP BY TGP_FACTORY_KEY, TGP_DATE, TGP_ROLLING_DATE, TGP_SCENARIO_ID;

	EXEC PROC_FACT_CUR_INCOME_STATEMENT;
END
GO

