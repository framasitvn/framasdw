

BEGIN TRAN
	INSERT INTO TBL_MR_SALES (MRS_FACTORY_ID, MRS_INDEX, MRS_PROJECT_NO, MRS_SALE_DATE, MRS_DATA_TYPE, MRS_SALE_VALUE, MRS_ROLLING_DATE, MRS_SCENARIO_ID)
	SELECT FACTORY_KEY FACTORY_ID, ID MRS_INDEX, [PRODUCT NO#] MRS_PROJECT_NO
			, CONVERT(DATE, CAST(YEAR AS NVARCHAR(4)) + RIGHT('00'+CAST(MONTH AS NVARCHAR(2)),2) +'01', 112) MRS_SALE_DATE
			, [DESCRIPTION] MRS_DATA_TYPE, [VALUE] MRS_SALE_VALUE
			, CONVERT(DATE, CAST(YEAR AS NVARCHAR(4))+'0101', 112) MRS_ROLLING_DATE
			, 2  MRS_SCENARIO_ID 
	FROM [MR Sales]..TBL_BUDGET
	WHERE [VALUE] > 0
	ORDER BY FACTORY_KEY, MONTH

	SELECT * FROM TBL_MR_SALES WHERE YEAR(MRS_SALE_DATE) = 2019 AND MRS_SCENARIO_ID = 2;

ROLLBACK

-- ABBVIE
