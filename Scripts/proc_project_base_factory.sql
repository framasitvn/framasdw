IF EXISTS (SELECT NULL FROM SYS.PROCEDURES WHERE NAME = 'PROC_PROJECT_BASE_FACTORY')
	DROP PROC PROC_PROJECT_BASE_FACTORY;
GO


CREATE PROC PROC_PROJECT_BASE_FACTORY @P_PROJECT_BASE NVARCHAR(100) AS
	DECLARE @UPD_SQL NVARCHAR(MAX);
	DECLARE @INS_SQL NVARCHAR(MAX);
BEGIN
	SET @UPD_SQL = 'UPDATE PBF 
		SET 
			PBF.[Project Name]		 = 	 A.[Project Name],
			PBF.[Project Number]	 = 	 A.[Project Number],
			PBF.[Tooling No#]		 = 	 A.[Tooling No#],
			PBF.[PIC]				 = 	 A.[PIC],
			PBF.[Silo]				 = 	 A.[Silo],
			PBF.[Status]			 = 	 A.[Status],
			PBF.[RemarkS]			 = 	 A.[Remarks],
			PBF.[Sizes]				 = 	 A.[Sizes],
			PBF.[Molds]				 = 	 A.[Molds],
			PBF.[Brands]			 = 	 A.[Brands],
			PBF.[Category]			 = 	 A.[Category],
			PBF.[Product Group]		 = 	 A.[Product Group],
			PBF.[Factory]			 = 	 A.[Factory],
			PBF.[Description]		 = 	 A.[Description],
			PBF.[New/Replacement]	 = 	 A.[New/Replacement],
			PBF.[Development Date]	 = 	 A.[Development Date],
			PBF.MoldMakingXX	 = 	 A.[Mold_Making Date],
			PBF.[MassProduction Date]= 	 A.[MassProduction Date],
			PBF.[Shoe Factory]		 = 	 A.[Shoe Factory],
			PBF.[CREATED_DATE]		 = 	 A.[CREATED_DATE],
			PBF.[MODIFIED_DATE]		 = 	 A.[MODIFIED_DATE]

		FROM tbl_ProjectBase_'+LTRIM(RTRIM(@P_PROJECT_BASE))+' PBF 
			INNER JOIN ##PROJECT_BASE_FACTORY A ON PBF.ID = A.ID;'
	SET @INS_SQL = 'INSERT INTO tbl_ProjectBase_'+LTRIM(RTRIM(@P_PROJECT_BASE))+' (
			[ID],
			[Project Name],
			[Project Number],
			[Tooling No#],
			[PIC],
			[Silo],
			[Status],
			[RemarkS],
			[Sizes],
			[Molds],
			[Brands],
			[Category],
			[Product Group],
			[Factory],
			[Description],
			[New/Replacement],
			[Development Date],
			MoldMakingXX,
			[MassProduction Date],
			[Shoe Factory],
			[CREATED_DATE],
			[MODIFIED_DATE])
		SELECT 
			A.[ID],
			A.[Project Name],
			A.[Project Number],
			A.[Tooling No#],
			A.[PIC],
			A.[Silo],
			A.[Status],
			A.[RemarkS],
			A.[Sizes],
			A.[Molds],
			A.[Brands],
			A.[Category],
			A.[Product Group],
			A.[Factory],
			A.[Description],
			A.[New/Replacement],
			A.[Development Date],
			A.[Mold_Making Date],
			A.[MassProduction Date],
			A.[Shoe Factory],
			A.[CREATED_DATE],
			A.[MODIFIED_DATE] 
		FROM ##PROJECT_BASE_FACTORY A
		WHERE NOT EXISTS (SELECT NULL FROM tbl_ProjectBase_'+LTRIM(RTRIM(@P_PROJECT_BASE))+' PBF WHERE A.[ID] = PBF.[ID])';

	IF UPPER(LTRIM(RTRIM(@P_PROJECT_BASE))) IN ('FH', 'FK', 'FKV', 'FZ', 'INL')
	BEGIN
		SET @UPD_SQL = REPLACE(@UPD_SQL, 'PBF.MoldMakingXX','PBF.[Mold_Making Date]');
		SET @INS_SQL = REPLACE(@INS_SQL, 'MoldMakingXX', '[Mold_Making Date]');
	END
	ELSE IF UPPER(LTRIM(RTRIM(@P_PROJECT_BASE))) IN ('FVN', 'GER')
	BEGIN
		SET @UPD_SQL = REPLACE(@UPD_SQL, 'PBF.MoldMakingXX','PBF.[Moldmaking start]');
		SET @INS_SQL = REPLACE(@INS_SQL, 'MoldMakingXX', '[Moldmaking start]');
	END

	--IF UPPER(LTRIM(RTRIM(@P_PROJECT_BASE))) = 'FH' 
	--BEGIN
	--	SET @UPD_SQL = REPLACE(@UPD_SQL, 'PBF.MoldMakingXX','PBF.Mold_Making Date');
	--	SET @INS_SQL = REPLACE(@INS_SQL, 'MoldMakingXX', '[Mold_Making Date]');
	--END
	--ELSE IF UPPER(LTRIM(RTRIM(@P_PROJECT_BASE))) = 'FK' 
	--BEGIN
	--	SET @UPD_SQL = REPLACE(@UPD_SQL, 'PBF.MoldMakingXX','PBF.Mold_Making Date');
	--	SET @INS_SQL = REPLACE(@INS_SQL, 'MoldMakingXX', '[Mold_Making Date]');
	--END
	--ELSE 
	--ELSE IF UPPER(LTRIM(RTRIM(@P_PROJECT_BASE))) = 'FZ' 
	--BEGIN
	--	SET @UPD_SQL = REPLACE(@UPD_SQL, 'PBF.MoldMakingXX','PBF.Mold_Making Date');
	--	SET @INS_SQL = REPLACE(@INS_SQL, 'MoldMakingXX', '[Mold_Making Date]');
	--END
	--ELSE IF UPPER(LTRIM(RTRIM(@P_PROJECT_BASE))) = 'GER' 
	--BEGIN
	--	SET @UPD_SQL = REPLACE(@UPD_SQL, 'PBF.MoldMakingXX','PBF.[Moldmaking start]');
	--	SET @INS_SQL = REPLACE(@INS_SQL, 'MoldMakingXX', '[Moldmaking start]');
	--END
	--ELSE IF UPPER(LTRIM(RTRIM(@P_PROJECT_BASE))) = 'INL' 
	--BEGIN
	--	SET @UPD_SQL = REPLACE(@UPD_SQL, 'PBF.MoldMakingXX','PBF.Mold_Making Date');
	--	SET @INS_SQL = REPLACE(@INS_SQL, 'MoldMakingXX', '[Mold_Making Date]');
	--END
	
	--PRINT @UPD_SQL;
	--PRINT '----';
	--PRINT @INS_SQL;
	EXEC sp_sqlexec @UPD_SQL;
	EXEC sp_sqlexec @INS_SQL;

END